import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:posts_app/Login.dart';
import 'package:posts_app/customWidgets/PostView.dart';
import 'dart:convert' as convert;

import 'models/Post.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Posts App',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Login(title: 'Posts App'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.userId,this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final int userId;
  final String title;



  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
   String token = "1cc03b74766b398149f5e0e09ceb72d892c038d343b3e996d45b6bd3309eac58";
    Future futurePost;

  bool _validate = false;

  @override
  void initState() {
    super.initState();
    futurePost = getAllPosts();
  }
  final TextEditingController _titleController = TextEditingController();
  final TextEditingController _bodyController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: FutureBuilder(
        builder: (context, snapshot) {
          // WHILE THE CALL IS BEING MADE AKA LOADING
          if (ConnectionState.active != null && !snapshot.hasData) {
            print(snapshot);
            return Center(child: CircularProgressIndicator());
          }

          // WHEN THE CALL IS DONE BUT HAPPENS TO HAVE AN ERROR
          if (ConnectionState.done != null && snapshot.hasError) {
            return Center(child: Text(snapshot.error));
          }
          if(snapshot.data==null){
            print("it is empty");
          }

          // IF IT WORKS IT GOES HERE!
          return ListView.separated(
            padding: EdgeInsets.all(10),
            itemCount: snapshot.data.length,
            itemBuilder: (context, index) {
              return PostView(id:snapshot.data[index]["id"],
                  userId:snapshot.data[index]["userId"],
                  title: snapshot.data[index]["title"],
                  body: snapshot.data[index]["body"]
              );
            },
            separatorBuilder: (BuildContext context, int index) {
              return Divider(
                indent: 20,
                endIndent: 20,
              );
            },
          );
        },
        future: futurePost,
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => _displayDialog(context),
          tooltip: 'Add Post',
          child: Icon(Icons.add)),
    );
  }
  void _addPostItem() async {
    // Wrapping it inside a set state will notify
    // the app that the state has changed

      var url =Uri.parse('https://gorest.co.in/public/v1/users/${widget.userId}/posts');
      try {
        var response =
            await http.post(url,headers: { HttpHeaders.authorizationHeader: "Bearer "+token
            },body: {
            "user_id": widget.userId.toString(),'title': _titleController.text,"body": _bodyController.text
            }
            );
        print(widget.userId);
        if(response.statusCode==201){
          _titleController.clear();
          _bodyController.clear();
          setState(() {
            futurePost = getAllPosts();
          });
        }

      } catch (error) {
        print(error);
        throw error;
      }
  }

  Future<AlertDialog> _displayDialog(BuildContext context) async {
    // alter the app state to show a dialog
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: const Text('Add a Post to your list'),
            content: Column(
              children: [
                TextField(
                  controller: _titleController,
                  decoration: const InputDecoration(hintText: 'Enter title here',),
                  autofocus: true,
                ),
                TextField(
                  controller: _bodyController,
                  autofocus: true,
                  decoration: const InputDecoration(hintText: 'Enter body here'),
                ),
              ],
            ),
            actions: <Widget>[
              // add button
              FlatButton(
                child: const Text('ADD'),
                onPressed: () {
                  //Navigator.of(context).pop();
                  _addPostItem();
                },
              ),
              // Cancel button
              FlatButton(
                child: const Text('CANCEL'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        }
    );
  }
 bool validators(value) {
   setState(() {
     return value.isEmpty ? _validate = true : _validate = false;
   });

 }
  Future getAllPosts() async {
    var url =Uri.parse('https://gorest.co.in/public/v1/users/${widget.userId}/posts');
    try {
      var response =
      await http.get(url);

      var data = convert.jsonDecode(response.body);
      print(data["data"]);
      return data["data"];
    } catch (error) {
      throw error;
    }
  }
}
