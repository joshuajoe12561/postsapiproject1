import 'package:flutter/material.dart';

class PostView extends StatelessWidget {
  PostView({
     this.id,
     this.userId,
     this.title,
     this.body,
    this.status
  }) : super(key: ObjectKey(id));
 final int id;
  final int userId;
  final String title;
  final String body;
  final String status;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        children: [
          ListTile(
            onTap: () {

            },
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              child: Text('$id'),
            ),
            title: Text(title),
          ),
          const Divider(),
          Container(
            padding: const EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(8.0),
            ),
            child: Text(body,style: TextStyle(fontWeight: FontWeight.normal))
          ),

        ],
      ),
    );
  }
}