import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class MyTextField{

  static create(BuildContext context, TextEditingController controller, String text,
      List<FormFieldValidator> validators, TextInputAction inputAction, FocusNode focusNode,
      Function fieldSubmitted, {String initialValue, String hintText = '',
        TextInputType inputType = TextInputType.text,
        Function onChanged, int maxLines = 1, attribute, String errorText}) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 4.0),
      child: FormBuilderTextField(
          controller: controller,
          decoration: InputDecoration(
              labelText: text,
              hintText:hintText,
              isDense: true,
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.blue, width: 2.0),
              ),
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 2.0),
              ),
              focusedErrorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red, width: 2.0),
              ),
              errorText: errorText
          ),
          keyboardType: inputType,
          validator: FormBuilderValidators.compose(validators),
          textInputAction: inputAction,
          focusNode: focusNode,
          onSubmitted: fieldSubmitted,
          onChanged:onChanged,
          maxLines: maxLines,
          initialValue: initialValue,
          style: TextStyle(
              fontSize: 16.0,
              height: 1.0
          )
      ),
    );
  }
}

class MyInputTextField extends StatefulWidget {

  final TextEditingController controller;
  final String text;
  final List<FormFieldValidator> validators;
  final TextInputAction inputAction;
  final FocusNode focusNode;
  final Function fieldSubmitted;
  final String attribute;


  const MyInputTextField ({ Key key, this.controller,
    this.text,
    this.validators,
    this.inputAction,
    this.focusNode,
    this.fieldSubmitted,
    this.attribute}): super(key: key);

  @override
  _MyInputTextFieldState createState() => _MyInputTextFieldState();
}

class _MyInputTextFieldState extends State<MyInputTextField> {


  @override
  Widget build(BuildContext context) {
    return MyTextField.create(context, widget.controller, widget.text,
        widget.validators, widget.inputAction, widget.focusNode,
        widget.fieldSubmitted,
        attribute: widget.attribute,);
  }
}