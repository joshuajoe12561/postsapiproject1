// import 'package:flutter/material.dart';
// import 'package:posts_app/customWidgets/MyTextField.dart';
// import 'package:flutter_form_builder/flutter_form_builder.dart';
// import 'package:posts_app/models/Post.dart';
// class CreatePost extends StatefulWidget {
//
//   @override
//   CreatePostState createState() {
//     return CreatePostState();
//   }
// }
//
// // Create a corresponding State class.
// // This class holds data related to the form.
// class CreatePostState extends State<CreatePost> {
//   // Create a global key that uniquely identifies the Form widget
//   // and allows validation of the form.
//   //
//   // Note: This is a GlobalKey<FormState>,
//   // not a GlobalKey<MyCustomFormState>.
//   ScaffoldState scaffold;
//
//   TextEditingController _titleController = TextEditingController();
//   TextEditingController _bodyController = TextEditingController();
//
//   final FocusNode _titleFocus = FocusNode();
//   final FocusNode _bodyFocus = FocusNode();
//
//   @override
//   void initState() {
//     super.initState();
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     scaffold = Scaffold.of(context);
//
//     // Build a Form widget using the _formKey created above.
//     return  Card(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             mainAxisSize: MainAxisSize.min,
//             children: <Widget>[
//               titleTextField(),
//               bodyTextField(),
//             ],
//           ),
//           elevation: 2,
//         );
//   }
//
//   Padding titleTextField() {
//     return MyTextField.create(
//         context,
//         _titleController,
//         'title',
//         [FormBuilderValidators.required(context),],
//         TextInputAction.next,
//         _titleFocus, (term) {
//       _fieldFocusChange(context, _titleFocus, _bodyFocus);
//     },
//         attribute: 'title');
//   }
//   Padding bodyTextField() {
//     return MyTextField.create(
//         context,
//         _bodyController,
//         'body',
//         [FormBuilderValidators.required(context)],
//         TextInputAction.next,
//         _titleFocus, (term) {
//       _fieldFocusChange(context, _bodyFocus,null);
//     },
//         attribute: 'body');
//   }
//   _fieldFocusChange(
//       BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
//     currentFocus.unfocus();
//     if (currentFocus != _titleFocus) {
//       FocusScope.of(context).requestFocus(nextFocus);
//     }
//   }
// }
